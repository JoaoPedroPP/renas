import React from 'react';
import classes from './Analise.module.css';

const Analise = (props) => {

    let content = null;
    if (props.data === null) {
        content = (
            <div className="bx--row">
                <p className={classes.placeholder}>Clique no botão "Analisar" para ver como seu modelo está se saindo!</p>
            </div>
        )
    } else {
        console.log(props.data);
        content = (
            <>
                <div className="bx--row">
                    <p className={classes.data}>
                        {props.data.entities.map((entity, index) => {
                            let prefix = <><span className={classes.subtitle}>Entidades</span><br /></>;
                            if (index !== 0) {
                                prefix = <br />;
                            }
                            return (<>
                                {prefix}
                                <strong>Tipo</strong>: "{entity.type}", <strong>Nome</strong>: "{entity.text}", <strong>Confiança</strong>: {entity.confidence}
                            </>)
                        })}
                    </p>
                </div>
                <hr style={{ margin: "16px 0" }} />
                <div className="bx--row" style={{ marginTop: "8px" }}>
                    <p className={classes.data}>
                        {props.data.relations.map((relation, index) => {
                            let prefix = <><span className={classes.subtitle}>Relações</span><br /></>;
                            if (index !== 0) {
                                prefix = <br />;
                            };
                            return (<>
                                {prefix}
                                <strong>Tipo</strong>: "{relation.type}"<br /><strong>Entidades envolvidas</strong>: {relation.arguments.map((argument, index) => {
                                    if (index !== relation.arguments.length - 1) {
                                        return '"' + argument.text + '", ';
                                    } else {
                                        return '"' + argument.text + '"';
                                    };
                                })}
                                <br />
                                <strong>Confiança</strong>: {relation.score}
                                <br />
                            </>);
                        })}
                    </p>
                </div>
            </>
        );
    }

    return (
        <div className="bx--grid" style={{ marginTop: "16px" }}>
            <label className="bx--label">
                Análise
            </label>
            {content}
        </div>
    );
};

export default Analise;