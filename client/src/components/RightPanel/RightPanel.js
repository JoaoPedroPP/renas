import React, { useState } from 'react';
import classes from './RightPanel.module.css';
import { Button, Modal, TextInput, ToastNotification } from 'carbon-components-react';

const RightPanel = () => {

    const initialState = {
        showModal: false,
        pass: "",
        notification: false,
        msg: ""
    }

    const [state, setState] = useState(initialState);

    const onModalOpenHandler = () => {
        setState({ ...state, showModal: true });
    };

    const onSubmitHandler = async () => {
        const rawResponse = await fetch('/enviar', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify({
                pass: state.pass
            })
        });
        const content = await rawResponse.json();
        if (content.err === false) {
            setState({ ...state, showModal: false, notification: "success", msg: content.msg, pass: "" });
        } else {
            setState({ ...state, showModal: false, notification: "error", msg: content.msg, pass: "" });
        };
    };

    let modal;
    if (state.showModal === true) {
        modal = (
            <div className="modal-settings">
                <Modal
                    open
                    aria-label="Enviar desafio para análise"
                    iconDescription="Fechar"
                    primaryButtonText="Enviar"
                    secondaryButtonText="Cancelar"
                    modalHeading="Enviar desafio para análise"
                    style={{ textAlign: "left" }}
                    onRequestClose={() => setState({ ...state, showModal: false })}
                    onRequestSubmit={onSubmitHandler}
                >
                    <TextInput
                        // style={{ marginBottom: "16px" }}
                        id="senha"
                        type="password"
                        labelText="Senha para o desafio recebida por email"
                        placeholder="Digite sua senha..."
                        onChange={(e) => setState({ ...state, pass: e.target.value })}
                        value={state.pass}
                    />
                </Modal>
            </div>
        );
    } else {
        modal = null;
    };

    let notification;
    if (notification !== false) {
        switch (state.notification) {
            case "error":
                notification = (
                    <div className="modal-settings">
                        <ToastNotification
                            kind="error"
                            title="Erro"
                            subtitle={state.msg}
                            iconDescription="Fechar"
                            onCloseButtonClick={() => setState({ ...state, notification: false, msg: "" })}
                            caption={Date()}
                        />
                    </div>
                );
                break;
            case "success":
                notification = (
                    <div className="modal-settings">
                        <ToastNotification
                            kind="success"
                            title="Sucesso"
                            subtitle={state.msg}
                            iconDescription="Fechar"
                            onCloseButtonClick={() => setState({ ...state, notification: false, msg: "" })}
                            caption={Date()}
                        />
                    </div>
                );
                break;
            default:
                notification = null;
                break;
        }
    } else {
        notification = null;
    }

    return (
        <div className={classes.container}>
            {modal}
            {notification}
            <h3>Desafio Natalino <span role="img" aria-label="natal">🎄</span></h3>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O Papai Noel está recebendo muitas cartas esse ano. Facilite o trabalho do Papai Noel criando um anotador de cartas de natal, para ele saber rapidamente qual presente entregar, para quem e onde!</p>
            <Button
                onClick={onModalOpenHandler}
            >Enviar</Button>
        </div>
    );
};

export default RightPanel;