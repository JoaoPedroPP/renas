import React, { useState } from 'react';
import {
    Header,
    HeaderName,
    HeaderGlobalBar,
    HeaderGlobalAction,
    HeaderPanel
} from 'carbon-components-react/lib/components/UIShell';
import { Modal, TextInput, ToastNotification } from 'carbon-components-react';
import GithubIcon32 from '@carbon/icons-react/lib/logo--github/32';
import SettingsIcon32 from '@carbon/icons-react/lib/settings/32';
import RightPanel from '../RightPanel/RightPanel';

const AppHeader = (props) => {

    const initialState = {
        showModal: false,
        notification: false,
        errMsg: "",
        credentials: {
            apikey: "",
            url: "",
            model_id: ""
        }
    };

    const [state, setState] = useState(initialState);

    const onChangeCredentialsHandler = async () => {
        props.setLoading();
        setState({ ...state, credentials: { ...state.credentials }, showModal: false });
        const rawResponse = await fetch('/credenciais', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify({
                ...state.credentials
            })
        });
        const content = await rawResponse.json();
        props.cancel();
        if (content.err === true) {
            setState({ showModal: false, notification: "error", errMsg: content.msg });
        } else {
            setState({ showModal: false, notification: "success" });
        };
    };

    let modal;
    if (state.showModal === true) {
        modal = (
            <div className="modal-settings">
                <Modal
                    open
                    aria-label="Trocar modelo de anotação"
                    iconDescription="Fechar"
                    primaryButtonText="Trocar modelo"
                    secondaryButtonText="Cancelar"
                    modalHeading="Troque o seu modelo de anotação"
                    style={{ textAlign: "left" }}
                    onRequestClose={() => setState({ showModal: false })}
                    onRequestSubmit={onChangeCredentialsHandler}
                >
                    <TextInput
                        style={{ marginBottom: "16px" }}
                        id="apikey-nlu"
                        labelText="Apikey do serviço de Natural Language Understanding"
                        placeholder="Digite a apikey..."
                        onChange={(e) => setState({ ...state, credentials: { ...state.credentials, apikey: e.target.value } })}
                        value={state.credentials.apikey}
                    />
                    <TextInput
                        style={{ marginBottom: "16px" }}
                        id="url-nlu"
                        labelText="URL do serviço de Natural Language Understanding"
                        placeholder="Digite a url..."
                        onChange={(e) => setState({ ...state, credentials: { ...state.credentials, url: e.target.value } })}
                        value={state.credentials.url}
                    />
                    <TextInput
                        id="model-wks"
                        labelText="Model ID do serviço de Watson Knowledge Studio"
                        placeholder="Digite o model ID..."
                        onChange={(e) => setState({ ...state, credentials: { ...state.credentials, model_id: e.target.value } })}
                        value={state.credentials.model_id}
                    />
                </Modal>
            </div>
        );
    } else {
        modal = null;
    };

    let notification;
    if (notification !== false) {
        switch (state.notification) {
            case "error":
                notification = (
                    <div className="modal-settings">
                        <ToastNotification
                            kind="error"
                            title="Erro"
                            subtitle={state.errMsg}
                            iconDescription="Fechar"
                            onCloseButtonClick={() => setState({ ...state, notification: false, errMsg: "" })}
                            caption={Date()}
                        />
                    </div>
                );
                break;
            case "success":
                notification = (
                    <div className="modal-settings">
                        <ToastNotification
                            kind="success"
                            title="Sucesso"
                            subtitle="Modelo alterado com sucesso."
                            iconDescription="Fechar"
                            onCloseButtonClick={() => setState({ ...state, notification: false })}
                            caption={Date()}
                        />
                    </div>
                );
                break;
            default:
                notification = null;
                break;
        }
    } else {
        notification = null;
    }

    return (
        <Header aria-label="IBM Digital Chistmas Challenge">
            {modal}
            {notification}
            <HeaderName href="/" prefix="IBM">
                Digital Christmas Challenge
            </HeaderName>
            <HeaderGlobalBar>
                <HeaderGlobalAction aria-label="Github" onClick={() => window.open("https://github.com/404")}>
                    <GithubIcon32 />
                </HeaderGlobalAction>
                <HeaderGlobalAction aria-label="Settings" onClick={() => setState({ ...state, showModal: true, credentials: { ...state.credentials } })}>
                    <SettingsIcon32 />
                </HeaderGlobalAction>
            </HeaderGlobalBar>
            <HeaderPanel aria-label="description" expanded>
                <RightPanel />
            </HeaderPanel>
        </Header>
    );
};

export default AppHeader;