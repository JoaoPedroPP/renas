import React, { useState } from 'react';
import './App.css';
import { Content } from 'carbon-components-react/lib/components/UIShell';
import { ToastNotification } from 'carbon-components-react';
import Header from './components/AppHeader/AppHeader';
import Carta from './components/Carta/Carta';
import Analise from './components/Analise/Analise';
import io from 'socket.io-client';

const App = (props) => {

  const initialState = {
    data: null,
    loading: false,
    points: null
  };

  const [state, setState] = useState(initialState);

  const socket = io();
  socket.on('pontuacaoRecebida', obj => {
    console.log('pontuação recebida:', obj);
    setState({
      ...state,
      // data: { ...state.data },
      points: obj.msg,
      loading: false
    });
  });

  const setDataHandler = (data) => {
    setState({
      ...state,
      data: {
        entities: data.entities,
        relations: data.relations
      },
      loading: false
    });
  };

  const setLoadingHandler = () => {
    setState({
      ...state,
      data: null,
      loading: true
    });
  };

  const cancelLoadingHandler = () => {
    setState({
      ...state,
      loading: false
    });
  };

  let loadingComponent = null;
  if (state.loading === true) {
    loadingComponent = (
      <div className="loading--hover">
        <div data-loading className="bx--loading">
          <svg className="bx--loading__svg" viewBox="-75 -75 150 150">
            <title>Loading</title>
            <circle className="bx--loading__stroke" cx="0" cy="0" r="37.5" />
          </svg>
        </div>
      </div>
    );
  } else {
    loadingComponent = null;
  };

  let points;
  if (state.points !== null) {
    points = (
      <div className="modal-settings">
        <ToastNotification
          kind="success"
          title="Desafio concluído!"
          subtitle={state.points}
          iconDescription="Fechar"
          onCloseButtonClick={() => setState({ ...state, points: null, loading: false })}
          caption={Date()}
        />
      </div>
    );
  };

  return (
    <div className="App">
      <Header setLoading={setLoadingHandler} cancel={cancelLoadingHandler} />
      <Content style={{ marginRight: "16rem" }}>
        {points}
        <div className="bx--grid">
          {loadingComponent}
          <div className="bx--row" style={{ marginBottom: "32px" }}>
            <Carta setData={(d) => setDataHandler(d)} setLoading={setLoadingHandler} cancel={cancelLoadingHandler} />
          </div>
          <div className="bx--row">
            <Analise data={state.data} />
          </div>
        </div>
      </Content>
    </div>
  );
};

export default App;
