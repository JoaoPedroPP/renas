import React, { useState } from 'react';
import { Button, TextArea, ToastNotification } from 'carbon-components-react';
import cartas from '../../constants/cartas';

const Carta = (props) => {

    const initialState = {
        letterText: "",
        error: false,
        errMsg: ""
    };

    const [state, setState] = useState(initialState);

    const textChangeHandler = (e) => {
        setState({ letterText: e.target.value });
    };

    const randomButtonHandler = () => {
        const carta = cartas[Math.floor(Math.random() * cartas.length)];
        setState({ letterText: carta });
    };

    const analyzeButtonHandler = async () => {
        props.setLoading();
        const rawResponse = await fetch('/analise', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: JSON.stringify({
                carta: state.letterText
            })
        });
        const content = await rawResponse.json();
        if (content.err === true) {
            console.error(content);
            setState({
                ...state,
                error: true,
                errMsg: content.msg
            });
            props.cancel();
        } else {
            props.setData(content.result);
            setState({
                ...state,
                error: false,
                errMsg: "404 - Not Found"
            })
        };
    };

    let error;
    if (state.error === true) {
        error = (
            <div className="loading--hover">
                <ToastNotification
                    kind="error"
                    title="Erro"
                    subtitle={state.errMsg.split('\n').map(line => <>{line}<br /></>)}
                    iconDescription="Fechar"
                    onCloseButtonClick={() => setState({ ...state, error: false, errMsg: "" })}
                    caption={Date()}
                />
            </div>
        );
    } else {
        error = null;
    }

    return (
        <div className="bx--grid">
            {error}
            <div className="bx--row" style={{ justifyContent: "flex-start", marginBottom: "8px" }}>
                <Button
                    kind="secondary"
                    style={{ marginRight: "8px" }}
                    onClick={randomButtonHandler}>
                    Carta Aleatória
                </Button>
                <Button
                    onClick={analyzeButtonHandler}>
                    Analisar
                </Button>
            </div>
            <div className="bx--row">
                <TextArea
                    value={state.letterText}
                    cols={100}
                    labelText="Carta"
                    rows={8}
                    onChange={(e) => textChangeHandler(e)}
                    placeholder="Escreva uma carta para o Papai Noel..."
                />
            </div>
        </div>
    );
};

export default Carta;