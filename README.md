- Deploy

<div align="center">
    <p><b>🚨 Clique para fazer o DEPLOY da aplicação na IBM Cloud 🚨</b></p>
    <a target="blank" href="https://cloud.ibm.com/devops/setup/deploy?repository=https://gitlab.com/JoaoPedroPP/renas.git">
        <img src="https://cloud.ibm.com/devops/setup/deploy/button.png" alt="Deploy to IBM Cloud">
    </a>
</div>

